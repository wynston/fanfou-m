#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import web
import time
import redis
import kvdb

mask = 'webpy_session_'

class RedisStore(web.session.Store):
    def __init__(self, timeout=86400, initial_flush=False):
        self.db = redis.Redis(db=8)
        self.timeout = timeout
        if initial_flush:
            self.db.flushdb()
    
    def __contains__(self, key):
        return bool(self.db.get(mask+key))

    def __getitem__(self, key):
        data = self.db.get(mask+key)
        if data:
            self.db.expire(mask+key, self.timeout)
            return self.decode(data)
        else:
            raise KeyError

    def __setitem__(self, key, value):
        self.db.set(mask+key, self.encode(value))
        self.db.expire(mask+key, self.timeout)
                
    def __delitem__(self, key):
        self.db.delete(mask+key)

    def cleanup(self, timeout):
        # Nothing to do
        pass


class Ctx:
    def __getattr__(self, key):
        return kvdb.select('mobile_ctx', key)
        
    def __setattr__(self, key, val): 
        kvdb.insert('mobile_ctx', key, val)
ctx = Ctx()
    
class Client:
    def set(self, cid, name, client):
        kvdb.insert('mobile_client', cid, {'cid': cid, 'name': name, 'client': client})
            
    def get(self, cid):
        return kvdb.select('mobile_client', cid)
client = Client()

class Token:
    def set(self, sid, cid, client, token):
        kvdb.insert('mobile_token', sid, {'sid': sid, 'cid': cid, 'client': client, 'token': token})
            
    def get(self, sid):
        return kvdb.select('mobile_token', sid)
            
    def kill(self, sid):
        kvdb.delete('mobile_token', sid)        
token = Token()

class Conf:
    def set(self, fid, shield):
        kvdb.insert('mobile_conf', fid, {'fid': fid, 'shield': shield})

    def get(self, fid):
        return kvdb.select('mobile_conf', fid) or {'conf': 'null'}
conf = Conf()

class Trace:
    def set(self, referer, format_exc):
        kvdb.insert('mobile_trace', time.time(), {'atime': time.time(), 'referer': referer, 'format_exc': format_exc})

    def get(self):
        res = kvdb.select_all('mobile_trace')
        keys = sorted(res) 
        return kvdb.select_all('mobile_trace').get(keys[-1])
trace = Trace()