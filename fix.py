#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import web
import time

def session():
    return web.config.get('_session')

def truncate(obj, size=32):
    seq = sorted([v['exp'] for v in obj.values()])
    seq = seq[:-size] if len(seq) > size else []
    [obj.pop(k) for k, v in obj.items() if v['exp'] in seq]
    return obj

def privatemsg_kv(obj):
    k, v = None, {}
    if session().me['id'] == obj['sender_id']:
        sender = obj['recipient_id']
    else:
        sender = obj['sender_id']
    v = {
            'id': sender,
            'exp': time.time(),
        } 
    k1 = obj.get('in_reply_to', {}).get('id')
    k2 = obj.get('id')
    return k1, k2, v

def privatemsg(func):
    def result(*args):
        data = func(*args)
        privatemsg = session().get('privatemsg', {})
        for item in data:
            k1, k2, v = privatemsg_kv(item)
            if k1: privatemsg[k1] = v
            privatemsg[k2] = v
        session().privatemsg = truncate(privatemsg)
        return data
    return result

def statuses_kv(obj):
    k, v = None, {}
    if obj.get('repost_status_id'):
        v = {
            'id': obj['repost_user_id'],
            'name': obj['repost_screen_name'],
            'exp': time.time(),
        }
        k = obj['repost_status_id']
    elif obj.get('in_reply_to_status_id'):
        v = {
            'id': obj['in_reply_to_user_id'],
            'name': obj['in_reply_to_screen_name'],
            'exp': time.time(),
        }
        k = obj['in_reply_to_status_id']
    return k, v
    
def photo_kv(obj):
    k, v = None, {}
    if obj.get('photo'):
        if obj.get('repost_user_id'):
            v = {
                'id': obj['repost_user_id'],
                'name': obj['repost_screen_name'],
                'exp': time.time(),
            }
            k = obj['repost_status_id']
        else:
            v = {
                'id': obj['user']['id'],
                'name': obj['user']['name'],
                'exp': time.time(),
            }
            k = obj['id']
    return k, v
    
def statuses_photo(func):
    def result(*args):
        data = func(*args)
        photo = session().get('photo', {})
        statuses = session().get('statuses', {})
        for item in data:
            k, v = photo_kv(item)
            if k: photo[k] = v
            k, v = statuses_kv(item)
            if k: statuses[k] = v
        session().photo = truncate(photo)
        session().statuses = truncate(statuses)
        return data
    return result

def statuses_photo_one(func):
    def result(*args):
        data = func(*args)
        photo = session().get('photo', {})
        statuses = session().get('statuses', {})
        k, v = photo_kv(data)
        if k: photo[k] = v
        k, v = statuses_kv(data)
        if k: statuses[k] = v
        session().photo = truncate(photo)
        session().statuses = truncate(statuses)
        return data
    return result


def mentions_sub(s):
    return re.sub(r'<[^>]*>([^>]*)</a>', lambda m: m.groups()[0], s)
    
def mentions_kv(obj):
    k, v = None, {}
    if obj['user']['protected']:
        v = {
            'data': {
                'id': obj['id'], 
                'text': mentions_sub(obj['text']), 
                'user': {'name': obj['user']['name']}
            },
            'exp': time.time()
        }
        k = obj['id']
    return k, v
    
def mentions(func):
    def result(*args):
        data = func(*args)
        mentions = session().get('mentions', {})
        for item in data:
            k, v = mentions_kv(item)
            if k: mentions[k] = v
        session().mentions = truncate(mentions, 16)
        return data
    return result

def users_kv(obj):
    k = obj[1]['id']
    try:
        del obj[1]['status']
    except:
        pass
    v = {'data': obj, 'exp': time.time()}
    return k, v
    
def users(func):
    def result(*args):
        data = func(*args)
        users = session().get('users', {})
        k, v = users_kv(data)
        users[k] = v
        session().users = truncate(users, 8)
        return data
    return result