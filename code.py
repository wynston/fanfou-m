#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import os
import fix
import urls
import web
import json
import time
import uuid
import store
import fanfou
import urllib2
import hashlib
import random
import datetime
import mimetypes
import traceback
try:
    import sae.const 
except:
    pass
    
abspath = os.path.dirname(os.path.abspath(__file__))

class Utils:
    def uuid(self, d):
        s = ''.join([str(v) for v in d.values()])
        return str(uuid.uuid5(uuid.NAMESPACE_DNS, s))
    
    def to_utf8(self, d):
        for k, v in d.items():
            d[k] = v.encode('utf-8')
        return dict(d)
    
    def nonce(self, d=8):
        ''' return an random string.'''
        digs = range(48,58)+range(65,91)+range(97,123)
        chrs = [chr(random.choice(digs)) for i in range(d)]
        return ''.join(chrs)
    
    def format_time(self, s):
        then = datetime.datetime.strptime(s, '%a %b %d %H:%M:%S +0000 %Y')
        delta = datetime.datetime.utcnow() - then
        if delta.days < 1:
            if delta.seconds < 60:
                s = '%s 秒前' % delta.seconds
            elif 60 <= delta.seconds < 3600:
                s = '%.f 分钟前' % round(delta.seconds/60.0)
            else:
                s = '约 %.f 小时前' % round(delta.seconds/3600.0)
        else:
            s = (then + datetime.timedelta(hours=8)).strftime('%Y-%m-%d %H:%M')
        return s

    def format_birthday(self, s):
        year, month, day = s.encode('utf-8').split('-')
        s = ''
        if int(year): s += '%s 年' % year
        if int(month): s += ' %s 月' % int(month)
        if int(day): s += ' %s 日' % day
        return s

    def html_encode(self, s):
        el = {'&lt;': '<', '&gt;': '>', '&quot;': '"', '&amp;': '&'}
        for k, v in el.items():
            s = s.replace(k, v)
        return s

    def replace_kw(self, s):
        s = re.sub('<b>([^<]*)</b>', lambda m: '<strong>%s</strong>'%m.groups()[0], s)
        return s.replace('http://fanfou.com', '')

    def msg_raw_id(self, item):
        return item['repost_status_id'] if item.get('repost_status_id') else item['id']
        
    def pack_image(self, filename, image, status=None, file_type=None):
        # build the mulitpart-formdata body
        BOUNDARY = self.nonce(12)
        body = []
        if status:
            body.append('--' + BOUNDARY)
            body.append('Content-Disposition: form-data; name="status"')
            body.append('Content-Type: text/plain; charset=US-ASCII')
            body.append('Content-Transfer-Encoding: 8bit')
            body.append('')
            body.append(status)
        body.append('--' + BOUNDARY)
        body.append('Content-Disposition: form-data; name="photo"; filename="%s"' % filename)
        body.append('Content-Type: %s' % file_type)
        body.append('Content-Transfer-Encoding: binary')
        body.append('')
        body.append(image)
        body.append('--' + BOUNDARY + '--')
        body.append('')
        body = '\r\n'.join(body)
        # build headers
        headers = {
            'Content-Type': 'multipart/form-data; boundary=' + BOUNDARY,
            'Content-Length': len(body)
        }
        return body, headers

    def authorize(self):
        exclude = ('/callback', '/authorize', '/autologin', '/help', '/share', '/debug')
        if self.oauth_token(): return
        if web.ctx.env.get('PATH_INFO') in exclude: return
        if web.ctx.env.get('PATH_INFO').startswith('/v2'): return
        raise web.seeother('/authorize')

    def oauth_token(self):
        return session.get('token') and session.get('consumer')
        
    def request(self, url, method='GET', body={}, headers={}):
        client = session.get('client')
        if not client:
            token =  session.token
            consumer = session.consumer
            client = fanfou.OAuth(consumer, token)
            session.client = client
        return client.request(url, method, body, headers)
        
    def token_verify(self, data):
        session.consumer = data['client']
        session.token = data['token']
        self.request('/account/rate_limit_status')
        
    def source(self, cid):
        consumer = session.consumer
        self._client = fanfou.XAuth(consumer, 'apps.test', 'Lisp125036')
        body = {'status': 'test %s'%time.time()}
        resp = self._client.request('/statuses/update', 'POST', body)
        sour = json.loads(resp.read())['source']
        return re.search(r'<[^>]*>(.*)</a>', sour).groups()[0]
        
    @fix.statuses_photo
    def home_timeline(self, body={}):
        resp = self.request('/statuses/home_timeline', 'GET', body)
        return json.loads(resp.read())

    @fix.mentions
    @fix.statuses_photo
    def mentions(self, body={}):
        resp = self.request('/statuses/mentions', 'GET', body)
        return json.loads(resp.read())

    def mentions_show(self, body={}):
        return session.get('mentions')[body['id']]['data']

    @fix.privatemsg
    def privatemsg(self, body={}):
        resp = self.request('/direct_messages/inbox', 'GET', body)
        return json.loads(resp.read())
        
    @fix.privatemsg
    def privatemsg_sent(self, body={}):
        resp = self.request('/direct_messages/sent', 'GET', body)
        return json.loads(resp.read())
        
    def privatemsg_show(self, body={}):
        msgid = body['id']
        userid = session.privatemsg.get(msgid)['id'].encode('utf-8')
        body['id'] = userid
        body['page'] = 1
        while 1:
            data = self.privatemsg_con(body)
            dmid_list = [item['id'] for item in data]
            if msgid in dmid_list: break
            if not data: break
            body['page'] += 1
        return data[dmid_list.index(msgid)] if data else {}

    def privatemsg_new(self, body={}):
        self.request('/direct_messages/new', 'POST', body)

    def privatemsg_del(self, body={}):
        self.request('/direct_messages/destroy', 'POST', body)

    @fix.privatemsg
    def privatemsg_con(self, body={}):
        resp = self.request('/direct_messages/conversation', 'GET', body)
        return json.loads(resp.read())

    def friend_add(self, body={}):
        try:
            resp = self.request('/friendships/create', 'POST', body)
            http_code = resp.code
        except urllib2.HTTPError:
            http_code = 403
        return http_code

    def friend_remove(self, body={}):
        self.request('/friendships/destroy', 'POST', body)

    def friend_request(self, body={}):
        resp = self.request('/friendships/requests', 'GET', body)
        return json.loads(resp.read())

    def friend_accept(self, body={}):
        self.request('/friendships/accept', 'POST', body)

    def friend_deny(self, body={}):
        self.request('/friendships/deny', 'POST', body)

    def friend_acceptadd(self, body={}):
        self.request('/friendships/accept', 'POST', body)
        self.request('/friendships/create', 'POST', body)

    def friends(self, body={}):
        resp = self.request('/users/friends', 'GET', body)
        return json.loads(resp.read())

    def followers(self, body={}):
        resp = self.request('/users/followers', 'GET', body)
        return json.loads(resp.read())

    @fix.statuses_photo
    def browse(self, body={}):
        resp = self.request('/statuses/public_timeline', 'GET', body)
        return json.loads(resp.read())

    @fix.statuses_photo
    def search_public(self, body={}):
        resp = self.request('/search/public_timeline', 'GET', body)
        return json.loads(resp.read())
    
    @fix.statuses_photo
    def search_users(self, body={}):
        resp = self.request('/search/users', 'GET', body)
        return json.loads(resp.read())
    
    @fix.statuses_photo
    def search_user_tl(self, body={}):
        resp = self.request('/search/user_timeline', 'GET', body)
        return json.loads(resp.read())

    @fix.statuses_photo
    def space(self, body={}):
        resp = self.request('/statuses/user_timeline', 'GET', body)
        return json.loads(resp.read())

    def update(self, body={}, headers={}):
        self.request('/statuses/update', 'POST', body, headers)

    def update_profile(self, body={}):
        self.request('/account/update_profile', 'POST', body)

    @fix.statuses_photo
    def album(self, body={}):
        resp = self.request('/photos/user_timeline', 'GET', body)
        data = json.loads(resp.read())
        self.album_nav(data)
        return data

    def album_nav(self, data):
        if data:
            photo = session.get('photo', {})
            k, v = fix.photo_kv(data[-1])
            photo[k] = v
            session.photo = fix.truncate(photo)
            #fix.statuses_photo(data)
            self.photo_navigate(data[-1]['id'])
        
    @fix.statuses_photo
    def photo(self, body={}):
        resp = self.request('/photos/user_timeline', 'GET', body)
        return json.loads(resp.read())

    def photo_upload(self, body={}, headers={}):
        self.request('/photos/upload', 'POST', body, headers)

    def photo_navigate(self, msgid):
        photo = session.get('photo', {})
        userid = photo[msgid]['id'].encode('utf-8')
        photo_list = session.get('photo_list', {})
        msgid_list = photo_list.get(userid, [])
        body = {'id': userid, 'format': 'lite', 'page': 1, 'count': 60}
        if msgid_list:
            data = self.photo(body)
            if data[0]['id'] != msgid_list[0]:
                msgid_list = [item['id'] for item in data]
        stop = int(len(msgid_list)/60.0)
        msgid_list = msgid_list[:stop*60]
        body['page'] = stop
        while not msgid in msgid_list or msgid == msgid_list[-1]:
            body['page'] += 1
            data = self.photo(body)
            msgid_list.extend([item['id'] for item in data])
            if not data: break
            
        idx = msgid_list.index(msgid)
        lastid = msgid_list[idx-1] if msgid != msgid_list[0] else 0
        nextid = msgid_list[idx+1] if msgid != msgid_list[-1] else 0
        nav = (lastid, msgid, nextid)
        photo_list[userid] = msgid_list
        session.photo_list = photo_list
        return idx, nav

    
    @fix.users
    def user_show(self, body={}):
        resp = self.request('/users/show/:id', 'GET', body)
        data = json.loads(resp.read())
        visible = data['protected'] and data['following'] if data['protected'] else True
        visible = visible  or body['id'] == self.me()['id']
        return visible, data

    def users(self, body={}):
        user = session.get('users', {}).get(body['id'], {})
        return user.get('data') or self.user_show(body)
        
    @fix.statuses_photo_one
    def msg_show(self, body={}):
        try:
            resp = self.request('/statuses/show', 'GET', body)
            data = json.loads(resp.read())
        except urllib2.HTTPError, msg:
            session.http= str(msg)
            data = {}
        return data

    def msg_del(self, body={}):
        self.request('/statuses/destroy', 'POST', body)

    def msg_favorite_add(self, body={}):
        return self.request('/favorites/create/:id', 'POST', body)

    def msg_favorite_del(self, body={}):
        self.request('/favorites/destroy/:id', 'POST', body)

    @fix.statuses_photo
    def favorites(self, body={}):
        resp = self.request('/favorites/:id', 'GET', body)
        return json.loads(resp.read())

    def notice(self):
        notice = {}
        notice['now'] = self.now()
        notice['action'] = session.get('action')
        session.action = ''
        resp = self.request('/account/notification', 'GET', {'mode': 'lite'})
        notice.update(json.loads(resp.read()))
        notice['trends'] = self.trends()
        return notice

    def trends(self):
        if not store.ctx.trends or self.timed():
            resp = self.request('/trends/list', 'GET')
            data = json.loads(resp.read())
            store.ctx.trends = data['trends']
        return store.ctx.trends

    def me(self):
        if not session.get('me'):
            resp = self.request('/users/show', 'GET', {'mode': 'lite'})
            data = json.loads(resp.read())
            session.me = {'id': data['id'], 'name': data['name']}
        return session.me

    def shield(self):
        if not session.get('conf'):
            session.conf = store.conf.get(self.me()['id'])
        return session.conf.get('shield', [])
        
    def filter(self, s):
        for item in self.shield():
            return item in s

    def timed(self):
        now = datetime.datetime.now()
        return now.minute % 10 == 0

    def now(self):
        now = datetime.datetime.now()
        return now.strftime('%y/%m/%d %H:%M')

    def gif(self, item):
        return 'gif' if item['photo']['largeurl'][-3:].lower() == 'gif' else ''

    def notfound(self):
        referer = web.ctx.env.get('HTTP_REFERER', web.ctx.home)
        referer = referer.replace(web.ctx.home, '')
        return web.notfound(render.unrealized(self.me(), 404, referer, ''))

    def internalerror(self):
        session.traceback = traceback.format_exc()
        referer = web.ctx.env.get('HTTP_REFERER', web.ctx.home)
        if 'HTTPError: HTTP Error 404: Not Found' in session.traceback:
            referer = referer.replace(web.ctx.home, '')
            return web.notfound(render.unrealized(self.me(), 404, referer, ''))
        elif 'HTTPError: HTTP Error 401: Unauthorized' in session.traceback:
            session.kill()
            web.setcookie('sid', '', 2592000)
            return web.internalerror(render.unrealized(self.me(), 401, '/authorize', ''))
        elif 'HTTPError: HTTP Error 400: Bad Request' in session.traceback:
            resp = self.request('/account/rate_limit_status')
            data = json.loads(resp.read())
            referer = referer.replace(web.ctx.home, 'http://m.fanfou.com')
            reset_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data['reset_time_in_seconds']))
            return web.internalerror(render.unrealized(self.me(), 400, referer, reset_time))
        else:
            referer = referer.replace(web.ctx.home, '')
            store.trace.set(referer, session.traceback)
            return web.internalerror(render.unrealized(self.me(), 500, referer, ''))
    
utils = Utils()


class favicon_ico:
    def GET(self):
        web.header('Content-Type', 'image/x-icon;charset=utf-8')
        return open(os.path.join(abspath, 'favicon.ico'), 'rb').read()

class home:
    def GET(self, page=1):
        body = {'page': page, 'format': 'html', 'mode': 'lite', 'count': 15}
        return render.home(utils.me(), utils.home_timeline(body), int(page), utils.notice())

    def POST(self):
        wi = web.input()
        if not wi.get('content'):
            session.action = '消息发送失败，请输入消息内容！'
            raise web.seeother('/home')
        elif wi.get('content') == session.get('last'):
            session.action = '请勿发重复消息！'
            raise web.seeother('/home')
        session.last = wi.content
        body = {'status': wi.content.encode('utf-8')}
        utils.update(body)
        session.action = '发送成功！'
        raise web.seeother('/home')

class msg_reply:
    def GET(self, msg_id):
        body = {'id': msg_id, 'mode': 'lite'}
        data = utils.msg_show(body)
        if not data:
            data = utils.mentions_show(body)
        return render.msg_reply(utils.me(), data)

    def POST(self, msg_id):
        wi = web.input()
        body = {'status': wi.content.encode('utf-8'), 'in_reply_to_status_id': msg_id}
        utils.update(body)
        session.action = '发送成功！'
        raise web.seeother('/home')

class msg_forward:
    def GET(self, msg_id):
        body = {'id': msg_id, 'mode': 'lite'}
        data = utils.msg_show(body)
        if not data:
            data = utils.mentions_show(body)
        return render.msg_forward(utils.me(), data)

    def POST(self, msg_id):
        wi = web.input() 
        body = {'status': wi.content.encode('utf-8'), 'repost_status_id': msg_id}
        utils.update(body)
        session.action = '发送成功！'
        raise web.seeother('/home')

class msg_new:
    def GET(self, user_id):
        body = {'id': user_id.encode('utf-8')}
        return render.msg_new(utils.me(), utils.users(body)[1])

    def POST(self, user_id):
        wi = web.input()
        body = { 
            'status': wi.content.encode('utf-8'),
            'in_reply_to_user_id': user_id.encode('utf-8')
        }
        utils.update(body)
        session.action = '发送成功！'
        raise web.seeother('/home')

class msg_del:
    def GET(self, msg_id):
        referer = web.ctx.env.get('HTTP_REFERER', '/home')
        referer = referer.replace(web.ctx.home, '')
        return render.msg_del(utils.me(), msg_id, referer)

    def POST(self, msg_id):
        body = {'id': msg_id }
        utils.msg_del(body)
        session.action = '消息删除成功！'
        raise web.seeother(web.input().referer)

class msg_favorite_add:
    def GET(self, msg_id):
        body = {'id': msg_id}
        try:
            utils.msg_favorite_add(body)
        except urllib2.HTTPError:
            session.action = '收藏失败，此消息不公开。'
        referer = web.ctx.env.get('HTTP_REFERER', '/home')
        raise web.seeother(referer)

class msg_favorite_del:
    def GET(self, msg_id):
        body = {'id': msg_id}
        utils.msg_favorite_del(body)
        referer = web.ctx.env.get('HTTP_REFERER', '/home')
        raise web.seeother(referer)

class favorites:
    def GET(self, user_id, page=1):
        body = {'id': user_id, 'count': 15, 'page': page, 'mode': 'lite', 'format': 'html'}
        visible, show = utils.users({'id': user_id})
        data = utils.favorites(body) if visible else []
        return render.favorites(utils.me(), visible, show, data, int(page), utils.notice())

class friend_request:
    def GET(self, page=1):
        body = {'count': 10, 'page': page}
        return render.friend_request(utils.me(), utils.friend_request(body), int(page), utils.notice())

class friend_acceptadd:
    def GET(self, user_id):
        body = {'id': user_id.encode('utf-8')}
        return render.friend_acceptadd(utils.me(), utils.users(body)[1])

    def POST(self, user_id):
        body = {'id': user_id.encode('utf-8')}
        utils.friend_acceptadd(body)
        if utils.notice()['friend_requests']:
            raise web.seeother('/friend.request')
        else:
            raise web.seeother('/home')

class friend_accept:
    def GET(self, user_id):
        body = {'id': user_id.encode('utf-8')}
        return render.friend_accept(utils.me(), utils.users(body)[1])

    def POST(self, user_id):
        body = {'id': user_id.encode('utf-8')}
        utils.friend_accept(body)
        if utils.notice()['friend_requests']:
            raise web.seeother('/friend.request')
        else:
            raise web.seeother('/home')

class friend_deny:
    def GET(self, user_id):
        body = {'id': user_id.encode('utf-8')}
        return render.friend_deny(utils.me(), utils.users(body)[1])

    def POST(self, user_id):
        body = {'id': user_id.encode('utf-8')}
        utils.friend_deny(body)
        if utils.notice()['friend_requests']:
            raise web.seeother('/friend.request')
        else:
            raise web.seeother('/home')

class friend_add:
    def GET(self, user_id):
        referer = web.ctx.env.get('HTTP_REFERER', '/home')
        referer = referer.replace(web.ctx.home, '')
        body = {'id': user_id.encode('utf-8'), 'mode': 'lite'}
        return render.friend_add(utils.me(), utils.users(body)[1], referer)

    def POST(self, user_id):
        wi = web.input()
        body = {'id': user_id.encode('utf-8')}
        if utils.friend_add(body) == 403:
            session.action = '已向 %s 发出关注请求，请等待确认。' %wi.name.encode('utf-8')
        else:
            session.action = '你已成功关注 %s' %wi.name.encode('utf-8')
        raise web.seeother(wi.referer)

class friend_remove:
    def GET(self, user_id):
        body = {'id': user_id.encode('utf-8'), 'mode': 'lite'}
        return render.friend_remove(utils.me(), utils.users(body)[1])

    def POST(self, user_id):
        wi = web.input()
        body = {'id': user_id.encode('utf-8'), 'mode': 'lite'}
        utils.friend_remove(body)
        session.action = '已将 %s 从关注的人中删除。' %wi.name.encode('utf-8')
        raise web.seeother(wi.referer)

class friends:
    def GET(self, user_id=None, page=1):
        if not user_id: user_id = utils.me()['id'].encode('utf-8')
        body = {'id': user_id, 'mode': 'lite', 'count': 50, 'page': page}
        visible, show = utils.users({'id': user_id})
        data = utils.friends(body) if visible else []
        return render.friends(utils.me(), visible, show, data, int(page), utils.notice())

class followers:
    def GET(self, user_id=None, page=1):
        if not user_id: user_id = utils.me()['id']
        body = {'id': user_id, 'mode': 'lite', 'count': 50, 'page': page}
        visible, show = utils.users({'id': user_id})
        data = utils.followers(body) if visible else []
        return render.followers(utils.me(), visible, show, data, int(page), utils.notice())

class mentions:
    def GET(self, page=1):
        body = {'page': page, 'format': 'html', 'mode': 'lite', 'count': 15}
        return render.mentions(utils.me(), utils.mentions(body), int(page), utils.notice())

class privatemsg_viewreply:
    def GET(self, msg_id):
        body = {'id': msg_id, 'mode': 'lite', 'count': 60}
        return render.privatemsg_viewreply(utils.me(), utils.privatemsg_show(body), utils.notice())

class privatemsg:
    def GET(self, page=1):
        body = {'page': page, 'format': 'html', 'mode': 'lite', 'count': 10}
        return render.privatemsg(utils.me(), utils.privatemsg(body), int(page), utils.notice())

class privatemsg_sent:
    def GET(self, page=1):
        body = {'page': page, 'format': 'html', 'mode': 'lite', 'count': 10}
        return render.privatemsg_sent(utils.me(), utils.privatemsg_sent(body), int(page), utils.notice())

class privatemsg_reply:
    def GET(self, msg_id):
        body = {'id': msg_id, 'count':60, 'mode': 'lite'}
        raw_dm = utils.privatemsg_show(body)
        return render.privatemsg_reply(utils.me(), raw_dm)

    def POST(self, msg_id):
        wi = web.input()
        body = { 
            'user': wi.sendto.encode('utf-8'),
            'text': wi.content.encode('utf-8'),
            'in_reply_to_id': msg_id
        }
        utils.privatemsg_new(body)
        session.action = '私信发送成功！'
        raise web.seeother('/home')

class privatemsg_create:
    def GET(self, user_id):
        user_id = user_id.encode('utf-8')
        referer = web.ctx.env.get('HTTP_REFERER', '/home')
        referer = referer.replace(web.ctx.home, '')
        return render.privatemsg_create(utils.me(), utils.users({'id':user_id})[1], referer, utils.notice())

    def POST(self, msg_id):
        wi = web.input()
        body = { 
            'user': wi.sendto.encode('utf-8'),
            'text': wi.content.encode('utf-8'),
        }
        utils.privatemsg_new(body)
        session.action = '私信发送成功！'
        raise web.seeother(wi.referer)

class privatemsg_del:
    def GET(self, msg_id):
        referer = web.ctx.env.get('HTTP_REFERER', '/home')
        referer = '/privatemsg/sent' if 'sent' in referer else '/privatemsg'
        return render.privatemsg_del(utils.me(), msg_id, referer)

    def POST(self, msg_id):
        wi = web.input()
        body = {'id': msg_id }
        utils.privatemsg_del(body)
        session.action = '私信删除成功！'
        raise web.seeother(wi.referer)

class statuses:
    def GET(self, msg_id):
        body = {'id': msg_id, 'mode': 'lite', 'format': 'html'}
        data = utils.msg_show(body)
        if data:
            return render.statuses(utils.me(), data, utils.notice())
        else:
            show = session.statuses.get(msg_id)
            if session.http == 'HTTP Error 403: Forbidden':
                return render.statuses_lock(utils.me(),show, utils.notice())
            elif session.http == 'HTTP Error 404: Not Found':
                return render.statuses_del(utils.me(),show, utils.notice())

class browse:
    def GET(self):
        body = {'count': 15, 'format': 'html'}
        return render.browse(utils.me(), utils.browse(body), utils.notice())

class album:
    def GET(self, user_id, page=1):
        if not user_id: user_id = utils.me()['id']
        user_id = user_id.encode('utf-8')
        body = {'id': user_id, 'count': 10, 'mode': 'lite', 'page': int(page)}
        visible, show = utils.users({'id': user_id})
        data = utils.album(body) if visible else []
        return render.album(utils.me(), visible, show, data, int(page), utils.notice())

class photo:
    def GET(self, mode, msgid):   
        body = {'id': msgid.encode('utf-8'), 'mode': 'lite', 'format': 'html'}
        data = utils.msg_show(body)
        userid = session.get('photo')[msgid]['id']
        if data:
            idx, nav = utils.photo_navigate(msgid)
            body = {'id': userid.encode('utf-8'), 'mode': 'lite', 'format': 'html'}
            return render.photo(utils.me(), utils.users(body)[1], data, idx, nav, mode, utils.notice())            
        else:
            show = session.photo.get(msgid)
            if session.http == 'HTTP Error 403: Forbidden':
                return render.photo_403(utils.me(), show, utils.notice())
            elif session.http == 'HTTP Error 404: Not Found':
                return render.photo_404(utils.me(), show, utils.notice())

class photo_upload:
    def GET(self):
        return render.photo_upload(utils.me(), utils.notice())

    def POST(self):
        wi = web.input(picture={})
        if wi.get('desc') == session.get('last'):
            session.action = '请勿发重复消息！'
            raise web.seeother('/photo.upload')
        if not wi.picture.filename:
            session.action = '请选择文件！'
            raise web.seeother('/photo.upload')
        file_type = mimetypes.guess_type(wi.picture.filename)[0]
        if file_type not in ['image/gif', 'image/jpeg', 'image/png', 'image/x-png']:
            session.action = '照片格式错误'
            raise web.seeother('/photo.upload')
        desc = wi.get('desc') or u'上传了新照片'
        body, headers = utils.pack_image(wi.picture.filename, wi.picture.file.read(), desc.encode('utf-8'), file_type)
        utils.photo_upload({'form-data': body}, headers)
        session.last = desc
        session.action = '发送成功！'
        raise web.seeother('/home')

class photo_del:
    def GET(self, msg_id):
        return render.photo_del(utils.me(), msg_id)

    def POST(self, msg_id):
        wi = web.input()
        body = {'id': msg_id }
        utils.msg_del(body)
        session.action = '照片删除成功！'
        raise web.seeother('/album')
    
class search:
    def GET(self):
        wi = web.input()
        if not wi.get('q'):
            return render.search(utils.me(), wi, (), utils.notice())
        elif wi.get('st') == '1':
            m = 'since_id' if wi.get('t') == '0' else 'max_id'
            body = {
                'q': wi.q.encode('utf-8'), 'format': 'html', 
                'count': 10, 'mode': 'lite', m: wi.get('m', '')
            }
            return render.search(utils.me(), wi, utils.search_user_tl(body), utils.notice())
        elif wi.get('st') == '2':
            p = int(wi.p) if wi.get('p') else 1
            body = {
                'q': wi.q.encode('utf-8'),  'format': 'html',
                'count': 10, 'mode': 'lite', 'page': p
            }
            return render.find(utils.me(), wi, utils.search_users(body), utils.notice())
        else:
            m = 'since_id' if wi.get('t') == '0' else 'max_id'
            body = {
                'q': wi.q.encode('utf-8'), 'format': 'html', 
                'count': 10, 'mode': 'lite', m: wi.get('m', '')
            }
            return render.search(utils.me(), wi, utils.search_public(body), utils.notice())

class query:
    def GET(self, word):
        word = word.replace('+', ' ')
        body = {'q': word.encode('utf-8'), 'format': 'html', 'count': 10, 'mode': 'lite'}
        return render.q(utils.me(), word, utils.search_public(body), utils.notice())

class space:
    def GET(self, user_id, page=1):
        visible, show = utils.users({'id': user_id.encode('utf-8') })
        #return show
        if visible:
            body = {'id': user_id.encode('utf-8'), 'page': page, 'format': 'html', 'mode': 'lite', 'count': 15}
            return render.space(utils.me(), show, utils.space(body), int(page), utils.notice())
        else:
            return render.space_lock(utils.me(), show, utils.notice())

class userview:
    def GET(self, user_id, page=1):
        visible, show = utils.users({'id': user_id.encode('utf-8') })
        if visible:
            body = {'id': user_id.encode('utf-8'), 'page': page, 'format': 'html', 'mode': 'lite', 'count': 15}
            return render.userview(utils.me(), show, utils.home_timeline(body), int(page), utils.notice())
        else:
            return render.space_lock(utils.me(), show, utils.notice())

class dialogue:
    def GET(self, user_id, page=1):
        visible, show = utils.users({'id': user_id.encode('utf-8') })
        if visible:
            body = {'id': user_id.encode('utf-8'), 'page': page, 'format': 'html', 'mode': 'lite', 'count': 15}
            return render.dialogue(utils.me(), show, utils.privatemsg_con(body), int(page), utils.notice())
        else:
            return render.space_lock(utils.me(), show, utils.notice())

class settings:
    def GET(self):
        cid = utils.uuid(session.consumer)
        source = store.client.get(cid)['name']
        body = {'id': utils.me()['id'].encode('utf-8')}
        bio = utils.users(body)[1]['description']
        shield = utils.shield()
        return render.settings(utils.me(), bio, ';'.join(shield), source, 0)
    
    def POST(self):
        wi = web.input()
        body = utils.to_utf8({'name': wi.name, 'description': wi.bio})
        utils.update_profile(body)
        cid = utils.uuid(session.consumer)
        source = store.client.get(cid)['name']
        shield = [i.strip() for i in wi.shield.split(';') if i.strip()]
        session.me.update({'name':wi.name})
        session.conf.update({'shield': shield})
        store.conf.set(session.me['id'], shield)
        return render.settings(utils.me(), wi.bio, wi.shield, source, 200)

class share_confirm:
    def GET(self):
        cid = utils.uuid(session.consumer)
        raise web.seeother('/share?cid=%s'%cid)

class share:
    def GET(self):
        referer = web.ctx.env.get('HTTP_REFERER')
        http_host = web.ctx.env.get('HTTP_HOST')
        cid =  web.input().get('cid').strip('/')
        if referer and 'settings' in referer:
            http_code = 200
        else:
            data = store.client.get(cid)
            try:                    
                callback = 'http://%s/callback' % http_host
                client = fanfou.OAuth(data['client'], callback=callback)
                session.consumer = data['client']
                session.request_token = client.request_token()
                raise web.seeother(client.authorize_url)
            except urllib2.HTTPError:
                http_code = 404 if data else 403
        return render.share(cid, http_code, http_host)

class autologin_confirm:
    def GET(self):
        sid = web.cookies().get('sid')
        raise web.seeother('/autologin?sid=%s'%sid)

class autologin:
    def GET(self):
        referer = web.ctx.env.get('HTTP_REFERER')
        http_host = web.ctx.env.get('HTTP_HOST')
        if referer and http_host in referer:
            return render.autologin()
        else:
            session.kill()
            sid =  web.input().get('sid').strip('/')
            data = store.token.get(sid)
            try:
                utils.token_verify(data)     # Verify the token
                web.setcookie('sid', sid, 2592000)
            except:
                web.setcookie('sid', '', 2592000)
                return render.authorize(404)
            raise web.seeother('/home')

class logout:
    def GET(self):
        session.kill()
        web.setcookie('sid', '', 2592000)
        return render.authorize(200)

class authorize:
    def GET(self):
        sid = web.cookies().get('sid')
        if web.input().get('m'):
            return render.authorize_d(200)
        if sid:
            data = store.token.get(sid)
            try:
                utils.token_verify(data)
                raise web.seeother('/home')
            except urllib2.HTTPError:
                pass
            except TypeError:
                pass
        return render.authorize(200)

    def POST(self):
        wi = web.input()
        http_host = web.ctx.env.get('HTTP_HOST')
        callback = 'http://%s/callback' % http_host
        if wi.get('user-defined'):
            consumer = utils.to_utf8({'key': wi.key, 'secret': wi.secret})      # from authorize_d.html
        else:
            consumer = store.client.get(wi.cid)['client']       # from authorize.html
        try:
            client = fanfou.OAuth(consumer, callback=callback)
            session.consumer = consumer
            session.request_token = client.request_token()
        except:
            return render.authorize_d(404)
        raise web.seeother(client.authorize_url)

class callback:
    def GET(self):
        consumer = session.consumer
        request_token = session.request_token
        if request_token:
            client = fanfou.OAuth(consumer, request_token)
            token = client.access_token()
            sid = utils.uuid(token)
            cid = utils.uuid(consumer)
            store.client.set(cid, utils.source(cid), consumer)
            store.token.set(sid, cid, consumer, token)
            session.client = client
            session.consumer = consumer
            session.token = token
            session.request_token = None
            web.setcookie('sid', sid, 2592000)
            raise web.seeother('/home')
        else:
            raise web.seeother('/authorize')

class help:
    def GET(self):
        topic = web.input().get('topic')
        try:
            return web.template.frender('templates/help_%s.html'%topic, globals=globals())()
        except:
            return web.template.frender('templates/help_notfound.html', globals=globals())()

class debug:
    def GET(self):
        return dict(session)
        
web.config.debug = True
app = web.application(urls.urls, globals()) 

if web.config.get('_session'):
    session = web.config._session
else:
    initializer = {'consumer': {}, 'token': {}, 'users': {}}
    session = web.session.Session(app, store.RedisStore(), initializer=initializer)
    web.config._session = session

app.notfound = utils.notfound
#app.internalerror = utils.internalerror
app.add_processor(web.loadhook(utils.authorize))
render = web.template.render(os.path.join(abspath, 'templates'), globals=globals())

if __name__ == '__main__':
    app.run()